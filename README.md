![Laravel Logo](public/image/laravel-logo.png)

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>


## Laravel Perpustakaan
### Things to do list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel-perpustakaan2.git`
2. Go inside the folder: `cd laravel-perpustakaan2`
3. Run `cp .env.example .env` then put your database name & credentials.
4. Run `composer install`
5. Run `php artisan key:generate`
6. Run `php artisan migrate`
7. Run `php artisan serve`
8. Open your favorite browser: http://localhost:8000

### Screen shot

Home Page

![Home Page](img/home.png "Home Page")

Add Anggota

![Add Anggota](img/add-anggota.png "Add Anggota")

List Anggota

![List Anggota](img/list-anggota.png "List Anggota")

Add Kategori Buku

![Add Kategori Buku](img/add-kategori.png "Add Kategori Buku")

List Kategori Buku

![List Kategori Buku](img/list-kategori.png "List Kategori Buku")

Add Buku

![Add Buku](img/add-buku.png "Add Buku")

List Buku

![List Buku](img/list-buku.png "List Buku")

Transaksi Peminjaman Buku

![Transaksi Peminjaman Buku](img/transaksi.png "Transaksi Peminjaman Buku")
